package document

import (
	"gitlab.com/prochac.dataddo/pseudo-commons/musthaveshit"
	"gitlab.com/prochac.dataddo/pseudo-commons/pgq"
)

func init() {
	_ = musthaveshit.EveryWantsThis
	_ = pgq.SomeWantThis
}

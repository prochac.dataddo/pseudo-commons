module gitlab.com/prochac.dataddo/pseudo-commons/document

go 1.20

replace (
	gitlab.com/prochac.dataddo/pseudo-commons => ../.
	gitlab.com/prochac.dataddo/pseudo-commons/pgq => ../pgq
)

require (
	gitlab.com/prochac.dataddo/pseudo-commons v0.0.0-00010101000000-000000000000
	gitlab.com/prochac.dataddo/pseudo-commons/pgq v0.0.0-00010101000000-000000000000
)

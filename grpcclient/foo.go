package grpcclient

import (
	"gitlab.com/prochac.dataddo/pseudo-commons/musthaveshit"
	grpc "gitlab.com/prochac.dataddo/pseudo-grpc-repo/gen/proto/go"
)

func init() {
	_ = musthaveshit.EveryWantsThis
	_ = grpc.SomeGRPC
}

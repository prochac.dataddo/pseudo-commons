module gitlab.com/prochac.dataddo/pseudo-commons/grpcclient

go 1.20

replace gitlab.com/prochac.dataddo/pseudo-commons => ../.

require (
	gitlab.com/prochac.dataddo/pseudo-commons v0.0.0-20230720110544-0f7c17e7ea3f
	gitlab.com/prochac.dataddo/pseudo-grpc-repo/gen/proto/go v0.0.0-20230720112714-bc89ba15b04a
)

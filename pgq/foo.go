package pgq

import "gitlab.com/prochac.dataddo/pseudo-commons/musthaveshit"

const SomeWantThis = 42

func init() {
	_ = musthaveshit.EveryWantsThis
}
